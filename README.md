# Lottery

# Task
Suppose we want to design a system that allows users to create and participate in lotteries. The rules of the lottery are as follows:
* The lottery has N participants and M tickets (M > N)
* Each participant can purchase one or more lottery tickets
* The winner of the lottery is chosen at random. However, each participant has a probability of winning the lottery that is proportional to the number of tickets she/he possesses. For example, let's assume we have three participants (N=3): P1 with two tickets, P2 with one ticket, and P3 with one ticket; then, P1 has a probability of winning the lottery equal to 50%, compared to 25% for P2 and P3.

Design and implement a program to handle this lottery system using your favorite programming language. Do not worry about writing code that resembles any particular language - we want to see the kind of code you would be able to write. The goal here is to assess your problem-solving skills.

Design and implement a set of test cases to verify if a given lottery system respects the rules. Introduce artificially some bugs into your implementation of the lottery system, so as to make the ticket extraction unfair/unbalanced, and check if your test cases are able to expose such faults.

# Files

## LotteryGame.py
This class creates a lottery game.
Running the file directly opens the GUI

Methods:
* start_game

## LotteryTest.py
This class creates a test for the lottery game, depending on the chosen bugs or test scenarios.

Methods:
* get_winners

# Dependencies
pysimplegui

matplotlib

numpy 