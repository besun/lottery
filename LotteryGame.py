import PySimpleGUI as sg
import random


class lottery_game:
    def __init__(self, participant_list, lottery_tickets, winner_number=1, bug=None):
        self.participant_list = participant_list
        self.lottery_tickets = lottery_tickets
        self.winner_number = winner_number
        self.bug = bug

    def start_game(self):
        if not self.bug == 3:
            if 0 in self.lottery_tickets:
                print('Error1: One or more participants have zero tickets')
                exit()
            elif len(self.lottery_tickets) > len(self.participant_list):
                print('Error2: There are more tickets entries than participants entries')
                exit()
            elif len(self.lottery_tickets) < len(self.participant_list):
                print('Error3: There are more participant entries than tickets entries')
                exit()
            elif not self.lottery_tickets and not self.participant_list:
                print('Error4: There are no tickets and no participants')
                exit()

        if self.bug == 1:
            winners = random.choices(self.participant_list, k=self.winner_number)
        elif self.bug == 2:
            winners = random.choices(self.participant_list, weights=sorted(self.lottery_tickets), k=self.winner_number)
        elif self.bug == 4:
            winners = random.choices(self.participant_list, weights=[x if x <4 else 3 for x in self.lottery_tickets], k=self.winner_number)
        else:
            winners = random.choices(self.participant_list, weights=self.lottery_tickets, k=self.winner_number)
        return winners

if __name__ == '__main__':
    sg.theme('DarkGrey5')  # Add a touch of color
    # All the stuff inside your window.
    exportCheck = False
    colSize1 = 18
    colSize2 = 40
    sg.SetOptions(element_padding=(5, 7))


    def get_layout_number():
        layout = [
            [sg.Text('Enter the number of participants.', size=(colSize2, 1), justification='center')],
            [sg.Text('', size=(colSize1, 2)), sg.Spin(values=[i for i in range(1,11)], key='participant_number', initial_value=1, size=(5, 2))],
            [sg.Text('', size=(10, 2)), sg.Button('Next', pad=(30, 0)), sg.Button('Cancel', pad=(30, 0))]
        ]
        return sg.Window('Lottery Game', layout)

    def get_layout_winner():
        layout = [
            [sg.Text('The winner is:', size=(20, 1), justification='center')],
            [sg.Text(winner[0], size=(20, 1), font=("Helvetica", 18), justification='center')],
            [sg.Text('', size=(10, 2)), sg.Button('New Round', pad=(30, 0)), sg.Button('Cancel', pad=(30, 0))]
        ]
        return sg.Window('Lottery Game', layout)

    def get_layout_game():
        layout = [
            [sg.Text('Participant Names', size=(20, 1)), sg.Text('Lottery Tickets', size=(15, 1))],
            *[[sg.Input('', size=(20, 1)), sg.Spin(values=[i for i in range(1, 11)], initial_value=1, size=(5, 1)), ]
              for i in range(participant_number)],
            [sg.Text('', size=(10, 2)), sg.Button('Play', pad=(30, 0)), sg.Button('Cancel', pad=(30, 0))]
        ]
        return sg.Window('Lottery Game', layout)

    # Create the Window
    # Event Loop to process "events" and get the "values" of the inputs
    new_round = True
    while new_round:
        layout_number = get_layout_number()
        while True:
            event, values = layout_number.read()
            if event == sg.WIN_CLOSED or event == 'Cancel':  # if user closes window or clicks cancel
                new_round = False
                break
            if event == 'Next':
                participant_number = int(values['participant_number'])
                layout_number.close()
                layout_game = get_layout_game()
                while True:
                    event, values = layout_game.read()
                    if event == sg.WIN_CLOSED or event == 'Cancel':  # if user closes window or clicks cancel
                        new_round = False
                        break
                    if event == 'Play':
                        # Save values to list
                        val = [v for v in values.values()]
                        lottery = lottery_game(val[::2], val[1::2])
                        # Determine the winner
                        winner = lottery.start_game()

                        layout_game.close()
                        layout_winner = get_layout_winner()
                        while True:
                            event, values = layout_winner.read()
                            if event == sg.WIN_CLOSED or event == 'Cancel':  # if user closes window or clicks cancel
                                new_round = False
                                break
                            if event == 'New Round':
                                break
                        layout_winner.close()
                        break
                break

