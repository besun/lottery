from LotteryGame import lottery_game
from collections import Counter
from matplotlib import pyplot as plt
import numpy as np
import random


class test:
    def __init__(self, participant_number, ticket_number, winner_number, bug):
        self.participant_number = participant_number
        self.ticket_number = ticket_number
        self.winner_number = winner_number
        self.bug = bug

    def get_winner_count(self, participant_list, winners):
        winners_count = sorted(Counter(winners).items())

        for p in participant_list:
            if p not in [w[0] for w in winners_count]:
                winners_count.append([p, 0])

        winners_count = sorted(winners_count, key=lambda x: x[0])
        winners_count = [x[1] for x in winners_count]
        return winners_count

    def get_distribution(self, winner_count):
        ticket_distribution = [x / sum(self.ticket_number) for x in self.ticket_number]
        winner_distribution = [x / sum(winner_count) for x in winner_count]
        return ticket_distribution, winner_distribution


    def get_winners(self):
        #Generate participant list
        number_list = [str(x) for x in range(1, self.participant_number+1)]
        participant_list = ['Participant'+x for x in number_list]

        #Create lottery game
        lottery = lottery_game(participant_list, self.ticket_number, winner_number=self.winner_number, bug=self.bug)

        #Get the amount of winners
        winners = lottery.start_game()
        winner_count = self.get_winner_count(participant_list, winners)

        # Get ticket and winner distribution
        ticket_distribution, winner_distribution = self.get_distribution(winner_count)

        return participant_list, ticket_distribution, winner_distribution

    def get_bar_plot(self, participant_list, ticket_distribution, winner_distribution, title, ax):
        w = 0.2
        bar1 = np.arange(len(participant_list))
        bar2 = [i + w for i in bar1]

        ax.bar(bar1, ticket_distribution, w, label='Ticket Distribution')
        ax.bar(bar2, winner_distribution, w, label='Winner Distribution')
        ax.set_title(title)
        ax.set_xticks(bar1 + w / 2)
        ax.set_xticklabels(bar1)
        ax.legend()
        return ax


winner_number = 100000

# bug = 1: Uniform distribution
# bug = 2: Sorted tickets
# bug = 3: Entry mismatch or zero ticket
# bug = 4: Max ticket number

bug = 2

# test scenario = 1: 4 plots (bug 1, 2 & 4)
# test scenario = 2: Participant without ticket (bug 3)
# test scenario = 2: Zero participants (bug 3)
# test scenario = 4: More participants than ticket entries (bug 3)

test_scenario = 1

if test_scenario == 1:
    # Test Scenario 1
    #Plot
    fig, axes = plt.subplots(2, 2, figsize=(10, 15))
    fig.suptitle('Test Scenario 1')

    title_test1 = 'Uniform'
    ticket_number_test1 = [1, 1, 1, 1, 1, 1, 1]
    participant_number_test1 = len(ticket_number_test1)
    test1 = test(participant_number_test1, ticket_number_test1, winner_number, bug)
    participant_list_test1, ticket_distribution_test1, winner_distribution_test1 = test1.get_winners()
    axes[0, 0] = test1.get_bar_plot(
        participant_list_test1, ticket_distribution_test1, winner_distribution_test1,
        title_test1, axes[0,0])

    title_test2 = 'Decreasing Distribution'
    ticket_number_test2 = [7, 6, 5, 4, 3, 2, 1]
    participant_number_test2 = len(ticket_number_test2)
    test2 = test(participant_number_test2, ticket_number_test2, winner_number, bug)
    participant_list_test2, ticket_distribution_test2, winner_distribution_test2 = test2.get_winners()
    axes[0, 1] = test2.get_bar_plot(
        participant_list_test2, ticket_distribution_test2, winner_distribution_test2,
        title_test2, axes[0, 1])

    title_test3 = 'Increasing Distribution'
    ticket_number_test3 = [1, 2, 3, 4, 5, 6, 7]
    participant_number_test3 = len(ticket_number_test3)
    test3 = test(participant_number_test3, ticket_number_test3, winner_number, bug)
    participant_list_test3, ticket_distribution_test3, winner_distribution_test3 = test3.get_winners()
    axes[1, 0] = test3.get_bar_plot(
        participant_list_test3, ticket_distribution_test3, winner_distribution_test3,
        title_test3, axes[1, 0])

    title_test4 = 'Random Distribution'
    ticket_number_test4 = []
    for i in range(1, 8):
        n = random.randint(1, 7)
        ticket_number_test4.append(n)
    participant_number_test4 = len(ticket_number_test4)
    test4 = test(participant_number_test4, ticket_number_test4, winner_number, bug)
    participant_list_test4, ticket_distribution_test4, winner_distribution_test4 = test4.get_winners()
    axes[1, 1] = test4.get_bar_plot(
        participant_list_test4, ticket_distribution_test4, winner_distribution_test4,
        title_test4, axes[1, 1])

    plt.show()

elif test_scenario == 2:
    # Test Scenario 2: Participant without ticket
    fig = plt.figure(figsize=(10, 15))
    fig.suptitle('Test Scenario 2')
    ax = plt.gca()
    title_test1 = 'Participant without ticket'
    ticket_number_test1 = [1, 0, 1, 1, 1, 1, 1]
    participant_number_test1 = len(ticket_number_test1)
    test1 = test(participant_number_test1, ticket_number_test1, winner_number, bug)
    participant_list_test1, ticket_distribution_test1, winner_distribution_test1 = test1.get_winners()
    print('Bug: Zero ticket participant not detected')
    ax = test1.get_bar_plot(
        participant_list_test1, ticket_distribution_test1, winner_distribution_test1,
        title_test1, ax)

    plt.show()

elif test_scenario == 3:
    # Test Scenario 3: Zero participants
    fig = plt.figure(figsize=(10, 15))
    fig.suptitle('Test Scenario 3')
    ax = plt.gca()
    title_test1 = 'No participants'
    ticket_number_test1 = [1, 1, 1, 1, 1, 1, 1]
    participant_number_test1 = 0
    test1 = test(participant_number_test1, ticket_number_test1, winner_number, bug)
    participant_list_test1, ticket_distribution_test1, winner_distribution_test1 = test1.get_winners()
    print('Bug: No participants')
    ax = test1.get_bar_plot(
        participant_list_test1, ticket_distribution_test1, winner_distribution_test1,
        title_test1, ax)

    plt.show()

elif test_scenario == 4:
    # Test Scenario 3: More participants than ticket entries
    fig = plt.figure(figsize=(10, 15))
    fig.suptitle('Test Scenario 3')
    ax = plt.gca()
    title_test1 = 'More participants than ticket entries'
    ticket_number_test1 = [1, 1, 1, 1, 1, 1, 1]
    participant_number_test1 = len(ticket_number_test1)+1
    test1 = test(participant_number_test1, ticket_number_test1, winner_number, bug)
    participant_list_test1, ticket_distribution_test1, winner_distribution_test1 = test1.get_winners()
    print('Bug: More participant entries than ticket entries')
    ax = test1.get_bar_plot(
        participant_list_test1, ticket_distribution_test1, winner_distribution_test1,
        title_test1, ax)

    plt.show()




